﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddForce : MonoBehaviour, IGrabber {

	public Transform target;
	public float force = 100;
	public float fuel = 100;
	public float increaseAmount = 10;
	float maxFuel;
	public Image img;
	Vector3 direction;

	Rigidbody rb;
	bool canShoot = false;
	void Awake()
	{
		maxFuel = fuel;
		rb = GetComponent<Rigidbody> ();
		Cursor.visible = false;
		Cursor.lockState = CursorLockMode.Confined;
	}

	void Shoot()
	{
		if (fuel > 0) {
			direction = Vector3.Normalize (transform.position - target.position);

			rb.AddForce (direction * force, ForceMode.Force);
			fuel -= 1;
		}
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.R)) {
			Application.LoadLevel (Application.loadedLevel);
		}
		if (img != null) {
			img.fillAmount = fuel / maxFuel;
		}
		if (Input.GetKey (KeyCode.Space)) {
			canShoot = true;
		}
		if (Input.GetKeyUp (KeyCode.Space)) {
			canShoot = false;
		}

		if (!canShoot && fuel < maxFuel) {
			fuel += (increaseAmount * Time.deltaTime);
		}
	
	}

	void FixedUpdate()
	{
		if (canShoot)
			Shoot ();
	}

	public void Pickup()
	{
		if (Random.value < 0.5f) {
			force += 200;	
		} else {
			fuel += 10;
		}
	}
}
