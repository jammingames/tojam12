﻿using System.Collections;
using UnityEngine;

public interface IGrabbable {

	void OnPickup (IGrabber grabber);

}

public interface IGrabber {
	void Pickup();
}
