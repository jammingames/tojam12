﻿using UnityEngine;
using System.Collections;

public class AddChildOnLevelLoad : MonoBehaviour
{

	public int desiredLevel;

	public string desiredGameObject;
	public Vector3 pos;
	public Vector3 rot;

	Vector3 origPos;
	Quaternion origRot;
	int origLevel;

	GameObject targetParent;
	Transform origParent;


	void Awake ()
	{
		origPos = transform.position;
		origRot = transform.rotation;
		if (transform.parent != null) {
			origParent = transform.parent;
		}
		origLevel = Application.loadedLevel;
	}

	void Reset ()
	{
		if (origParent != null) {
			transform.parent = origParent;
		} else {
			transform.parent = null;
		}
		transform.position = origPos;
		transform.rotation = origRot;
	}

	void OnLevelWasLoaded (int level)
	{
		if (level == desiredLevel) {
			if (GameObject.Find (desiredGameObject) != null) {
				targetParent = GameObject.Find (desiredGameObject);
				transform.parent = targetParent.transform;
			}
			transform.localPosition = pos;
			transform.localRotation = Quaternion.Euler (rot);
		} else if (level == origLevel) {
			Reset ();
		}
	}


}
