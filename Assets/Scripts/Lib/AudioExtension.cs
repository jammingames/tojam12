﻿using System.Collections;
using UnityEngine;

public static class AudioExtension
{

	public static IEnumerator PlayClip (this AudioSource audioSource, System.Action onComplete)
	{
		audioSource.Play ();

		while (audioSource.isPlaying) {
			yield return null;
		}

		if (onComplete != null)
			onComplete ();
	}

}
