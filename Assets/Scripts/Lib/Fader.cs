﻿using UnityEngine;
using System;
using System.Collections;

public class Fader : MonoBehaviour
{
	static Fader _instance = null;

	public static Fader instance {
		get {
			if (!_instance) {
				// check if an FIX_ME is already available in the scene graph
				_instance = FindObjectOfType (typeof(Fader)) as Fader;

				// nope, create a new one
				if (!_instance) {
					var obj = new GameObject ("Fader");
					_instance = obj.AddComponent<Fader> ();
				}
			}

			return _instance;
		}
	}

	void OnApplicationQuit ()
	{
		// release reference on exit
		_instance = null;
	}

	public void FadeInOut (Material fadeObj)
	{
		StartCoroutine (fadeObj.Fade (1.0f, 0.2f, 0.6f, 
			() => {

				StartCoroutine (fadeObj.Fade (0.1f, 1.0f, 0.6f, 
					() => {
						Debug.Log ("fade is finished");
						//Do things when fade is done
					}));
			}));
	}

	public void FadeAlphaIn (Material fadeObj, float duration, Action onComplete)
	{
		StartCoroutine (fadeObj.FadeAlpha (0, 1f, duration, onComplete));
	}

	public void FadeAlphaIn (Material fadeObj, float target, float duration, Action onComplete)
	{
		StartCoroutine (fadeObj.FadeAlpha (fadeObj.color.a, target, duration, onComplete));
	}

	public void FadeAlphaOut (Material fadeObj, float duration, Action onComplete)
	{
		StartCoroutine (fadeObj.FadeAlpha (1, 0f, duration, onComplete));
	}

	public void FadeAlphaOut (Material fadeObj, float target, float duration, Action onComplete)
	{
		StartCoroutine (fadeObj.FadeAlpha (fadeObj.color.a, target, duration, onComplete));
	}
}

