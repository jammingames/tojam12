﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LerpLight : MonoBehaviour
{

	Light mLight;
	public float duration;
	float origIntensity;
	public float minIntensity = 1;
	public float maxIntensity = 5;

	void Awake ()
	{
		mLight = GetComponent<Light> ();
	}


	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		mLight.intensity = (minIntensity + Mathf.PingPong (Time.time * duration, maxIntensity));
	}
}
