﻿using UnityEngine;

using System.IO;
using System.Collections;
using System.Collections.Generic;

public class Localizer : Singleton<Localizer>
{
	//External data
	[SerializeField]
	private string filepath;

	//Internal data
	Dictionary<string, string> localStrings = new Dictionary<string, string> ();

	private string language;

	//Getters
	public string Language {
		get {
			return language;
		}
	}

	void Awake ()
	{
		ParseKeysFile ();
	}

	public string GetLocalString (string key)
	{
		string result;

		if (localStrings.TryGetValue (key, out result)) {
			return result;
		} else {
			D.warn ("No local string with key: " + key);

			return "!!!" + key.ToUpper () + "!!!";
		}
	}

	private void ParseKeysFile ()
	{
		//string test = Application.streamingAssetsPath 
		SimpleJSON.JSONNode keysRoot = SimpleJSON.JSON.Parse (File.ReadAllText (Application.streamingAssetsPath + "/" + filepath));
		#if UNITY_EDITOR 
		keysRoot = SimpleJSON.JSON.Parse (File.ReadAllText (Application.streamingAssetsPath + "/" + filepath));
		#endif

		#if UNITY_IPHONE && !UNITY_EDITOR
		keysRoot = SimpleJSON.JSON.Parse(File.ReadAllText(Application.streamingAssetsPath + "/" + filepath));
		#endif

		language = keysRoot ["language"].Value;

		//Get the strings
		SimpleJSON.JSONNode stringsNode = keysRoot ["strings"];

		foreach (string key in stringsNode.Keys) {
			localStrings.Add (key, stringsNode [key].Value);
		}
	}

	private string[] ToStringArray (SimpleJSON.JSONArray nodes)
	{
		int numNodes = nodes.Count;

		string[] strArray = new string[numNodes];

		for (int i = 0; i < numNodes; ++i) {
			strArray [i] = nodes [i].Value;
		}

		return strArray;
	}
}
