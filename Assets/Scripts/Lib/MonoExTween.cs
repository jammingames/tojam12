﻿using UnityEngine;
using System.Collections;

public class MonoExTween : MonoEx
{
	public float duration = 2;
	public EaseType easer = EaseType.SmoothStepInOut;

	//use this in any spot you need to check if object shouldnt do something while in mid transition back to zero
	public bool inTransition = false;


	public override void Reset ()
	{
		StartCoroutine (MovementSequence ());

	}

	IEnumerator MovementSequence ()
	{
		inTransition = true;
		if (GetComponent<Collider> () != null) {
			GetComponent<Collider> ().enabled = false;
		}
		rb.isKinematic = true;
		rb.velocity = Vector3.zero;
		StartCoroutine (transform.RotateTo (GetOrigRot (), duration, easer, null));
		yield return StartCoroutine (transform.MoveTo (GetOrigPos (), duration, easer, null));
		if (GetComponent<Collider> () != null) {
			GetComponent<Collider> ().enabled = false;
		}
		inTransition = false;
		rb.isKinematic = false;
		if (GetComponent <Collider> () != null) {
			GetComponent<Collider> ().enabled = true;
		}
	}
}
