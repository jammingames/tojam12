﻿using UnityEngine;
using System.Collections;

public class Persist : MonoBehaviour
{
	

	void Awake ()
	{

		if (_instance != null && _instance != this) {
			print ("DESTROYING INSTANCE: " + _instance + "   " + gameObject.name);
			Destroy (this.gameObject);
			return;//Avoid doing anything else
		}
		_instance = this;
		DontDestroyOnLoad (gameObject);

	}


	private static Persist _instance = null;

	public static Persist instance {
		get {
			if (!_instance) {
				_instance = FindObjectOfType (typeof(Persist)) as Persist;
			}
			return _instance;

		}
	}



	void Update ()
	{
		#if UNITY_EDITOR
		if (Input.GetKey (KeyCode.Equals)) {
			Time.timeScale += 0.1f;
		}
		if (Input.GetKey (KeyCode.Minus) && Time.timeScale > 0.1f) {
			Time.timeScale -= 0.1f;
		}
		if (Input.GetKeyDown (KeyCode.P)) {
			Time.timeScale = 1;
		}
		#endif

		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}

	}


	void OnApplicationQuit ()
	{
		// release reference on exit
		_instance = null;
	}

}
