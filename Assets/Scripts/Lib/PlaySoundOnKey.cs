﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnKey : MonoBehaviour
{

	AudioSource audio;
	public KeyCode key = KeyCode.Space;

	void Awake ()
	{
		
		audio = GetComponent<AudioSource> ();
	}

	void Update ()
	{
		if (Input.GetKeyDown (key)) {
			audio.Play ();
		}
	}

}
