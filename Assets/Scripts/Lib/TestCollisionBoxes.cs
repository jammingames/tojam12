﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestCollisionBoxes : MonoBehaviour
{

	public Collider first;
	public Collider second;

	//THIS ASSUMES (0,0) == bottom left of screen
	bool CollisionTest (Collider a, Collider b)
	{
		var A = a.bounds;
		var B = b.bounds;

		//if left of A.x is within B.x
		if (A.min.x < B.max.x && A.min.x > B.min.x) {
			//if bottom A.y is within B.y
			if (A.min.y > B.min.y && A.min.y < B.max.y) {
				print ("Bottom A within b on y axis, and left is inside");
				return true;
			}
			//if top of A.y is within B.y
			if (A.max.y < B.max.y && A.max.y > B.min.y) {
				print ("Top A within b on y axis, and left is inside");
				return true;
			}
		}

		//if right of A.x is within B.x
		if (A.max.x > B.min.x && A.max.x < B.max.x) {
			//if top of A.y within B.y
			if (A.max.y > B.min.y && A.max.y < B.max.y) {
				print ("Top A within b on y axis, and RIGHT is inside");
				return true;
			}
			//if bot of A.y is within B.y
			if (A.min.y > B.min.y && A.min.y < B.max.y) {
				print ("Bottom A within b on y axis, and RIGHT is inside");
				return true;
			}
		}
		return false;
	}

	void Update ()
	{

		if (CollisionTest (first, second) == true) {
			first.GetComponent<MeshRenderer> ().material.color = Color.red;

		} else {
			first.GetComponent<MeshRenderer> ().material.color = Color.green;
		}

	}
}
