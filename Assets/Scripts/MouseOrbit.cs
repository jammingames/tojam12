﻿using UnityEngine;
using System.Collections;

public class MouseOrbit : MonoBehaviour {


	public Transform target;
	public Transform mesh;
	public Animator animObj;
	public float targetHeight = 1.7f;
	public float distance = 10.0f;
	public float maxDistance = 10.0f;
	public float minDistance = 1.0f;

	public float xSpeed = 250.0f;
	public float ySpeed = 120.0f;

	public int zoomRate = 40;
	public float rotationDampening = 3.0f;

	public float zoomDampening = 5.0f;


	public float yMinLimit = -20f;
	public float yMaxLimit = 80;
	public float speed = 3;
	private GameObject cam;
	private Color newColor;



	private float x = 0.0f;
	private float y = 0.0f;

	private float currentDistance;
	private float desiredDistance;
	private float correctedDistance;
	Rigidbody rigidbody;

	//@script AddComponentMenu("Camera-Control/Mouse Orbit")

	void Awake () {
		if (GetComponent<Rigidbody> () != null) {
			rigidbody = GetComponent<Rigidbody> ();
			
		}
	
	}	

	void Start () {


		Vector3 angles = transform.eulerAngles;
		x = angles.y;
		y = angles.x;

		currentDistance = distance;
		desiredDistance = distance;
		correctedDistance = distance;


		//cam = GameObject.Find("ThirdPersonCamera");


		// Make the rigid body not change rotation
		if (rigidbody)
			rigidbody.freezeRotation = true;
	}


	void FixedUpdate() {
		if (!target)
			return;
		if (Input.GetMouseButton(0))
			RotateMesh(speed*speed*speed);
	}


	void Update() {
		if (!target)
			return;


		if (Input.GetAxis("Horizontal") != 0 || (Input.GetAxis("Vertical")) != 0)
			RotateMesh(speed);

	}

	void LateUpdate () {
		if (!target) 
			return;

		x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
		y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;
		//target.transform.Rotate(0, Input.GetAxis("Mouse X") * xSpeed, 0);


		y = ClampAngle(y, yMinLimit, yMaxLimit);

		desiredDistance = Mathf.Clamp(desiredDistance, minDistance, maxDistance);
		correctedDistance = desiredDistance;       

		Quaternion rotation = Quaternion.Euler(y, x, 0);
		//Vector3 position = rotation * new Vector3(0.0f, 0.0f, -distance) + target.position;
		Vector3 position = target.position - (rotation * Vector3.forward * desiredDistance + new Vector3(0, -targetHeight, 0));

		RaycastHit collisionHit;

		Vector3 trueTargetPosition = new Vector3(target.position.x, target.position.y + targetHeight, target.position.z);



		// if there was a collision, correct the camera position and calculate the corrected distance

		bool isCorrected = false;

		if (Physics.Linecast(trueTargetPosition, position, out collisionHit))

		{

			position = collisionHit.point;

			correctedDistance = Vector3.Distance(trueTargetPosition, position);

			isCorrected = true;

		}



		// For smoothing, lerp distance only if either distance wasn't corrected, or correctedDistance is more than currentDistance

		currentDistance = !isCorrected || correctedDistance > currentDistance ? Mathf.Lerp(currentDistance, correctedDistance, Time.deltaTime * zoomDampening) : correctedDistance;
//		if (currentDistance <= 5f)
//		{
//			newColor = mesh.renderer.material.color;
//			newColor.a = 0.3f;	
//			mesh.renderer.material.color = newColor;
//		}
//		else
//		{
//			newColor = mesh.renderer.material.color; 
//			newColor.a = 1.0f;
//			mesh.renderer.material.color = newColor;
//		}
//

		// recalculate position based on the new currentDistance

		position = target.position - (rotation * Vector3.forward * currentDistance + new Vector3(0, -targetHeight, 0));




		transform.rotation = rotation;
		transform.position = position;
		//  animObj.SetLookAtPosition(target.position);


	}

	void RotateMesh(float rate) {

		Vector3 newRotation = Quaternion.LookRotation(transform.forward - target.transform.up).eulerAngles;
		newRotation.x = 0;
		newRotation.z = 0;
		target.transform.rotation = Quaternion.Slerp(target.transform.rotation, Quaternion.Euler(newRotation), Time.deltaTime * rate);
	}	


	static float ClampAngle (float angle, float min, float max) {
		if (angle < -360)
			angle += 360;
		if (angle > 360)
			angle -= 360;
		return Mathf.Clamp (angle, min, max);
	}

}