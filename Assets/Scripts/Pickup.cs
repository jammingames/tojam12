﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour, IGrabbable{



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}



	public void OnPickup (IGrabber grabber)
	{

	}


	void OnTriggerEnter(Collider col)
	{
		if (col.GetComponent<IGrabber> () != null) {
			col.GetComponent<IGrabber> ().Pickup ();
			GameObject.Destroy (gameObject);

		}
	}


}
